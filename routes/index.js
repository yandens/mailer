const router = require("express").Router();
const user = require("../controllers/user");
const middleware = require("../middleware/mustLogin");

router.post("/login", user.login);
router.get('/forgot-password-page', user.forgotPasswordPage)
router.post("/forgot-password", user.forgotPassword);
router.post("/reset-password", user.resetPassword);
router.get('/reset-password-page', user.resetPasswordPage)

module.exports = router;
